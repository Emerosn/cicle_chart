import 'package:flutter/material.dart';
import 'package:start/contoller/CircularProgress.dart';

class Start extends StatefulWidget {
  Start({Key? key}) : super(key: key);

  @override
  _StartState createState() => _StartState();
}

class _StartState extends State<Start> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  elevation: 1,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10)),
                    child: Container(
                      padding: EdgeInsets.only(top: 10),
                      color: Colors.amber,
                      child: (Container(
                        padding: EdgeInsets.all(5),
                        width: double.infinity,
                        height: 255,
                        color: Colors.white,
                        child: Column(
                          children: [
                            Expanded(child: Row()),
                            Divider(
                              color: Colors.grey,
                              height: 1,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.all(20),
                                        child: new RadialChart(
                                          percent: 10,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [],
                                )
                              ],
                            ),
                          ],
                        ),
                      )),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
