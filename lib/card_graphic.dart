import 'package:flutter/material.dart';
import 'package:start/contoller/CircularProgress.dart';

class CardGraphic extends StatefulWidget {
  /// *Valeu viwer Percent [Double]
  final double percent;

  ///
  final String texthead;
  final String textrequest;
  final int valuetotal;
  final int valueclosed;
  final int valueprevious;
  final String textclosed;
  final String total;
  final String previous;
  final double elevation;
  const CardGraphic({
    Key? key,
    this.valuetotal = 0,
    this.valueclosed = 0,
    this.valueprevious = 0,
    this.textrequest = '---',
    this.percent = 0.0,
    this.texthead = 'Text Example',
    this.textclosed = 'Text Example Closed',
    this.total = 'Text Example Total',
    this.previous = 'Text Example Previous',
    this.elevation = 1,
  }) : super(key: key);

  @override
  _CardGraphicState createState() => _CardGraphicState();
}

class _CardGraphicState extends State<CardGraphic> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      color: Colors.red,
      clipBehavior: CardTheme.of(context).clipBehavior,
      child: Container(
        width: 594,
        height: 491,
        margin: EdgeInsets.only(top: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10),
          ),
        ),
        child: Column(
          children: <Widget>[
            Row(
              verticalDirection: VerticalDirection.down,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 10),
                  padding: EdgeInsets.all(10),
                  child: Text(
                    this.widget.texthead,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
            Row(
              verticalDirection: VerticalDirection.up,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(10),
                      width: 150,
                      height: 150,
                      child: Column(
                        children: <Widget>[
                          Text(
                            this.widget.textclosed,
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Text(
                            this.widget.valueclosed.toString(),
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 58,
                              fontWeight: FontWeight.w700,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(10),
                      width: 150,
                      height: 150,
                      child: Column(
                        children: <Widget>[
                          Text(
                            this.widget.total,
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Text(
                            this.widget.valuetotal.toString(),
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 58,
                              fontWeight: FontWeight.w700,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.all(10),
                      width: 150,
                      height: 150,
                      child: Column(
                        children: [
                          Text(
                            this.widget.previous,
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 15,
                                fontWeight: FontWeight.w400),
                          ),
                          Text(
                            this.widget.valueprevious.toString(),
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 58,
                              fontWeight: FontWeight.w700,
                              color: Color.fromARGB(255, 0, 199, 82),
                            ),
                          ),
                          Container(
                            height: 25,
                            width: 50,
                            decoration: BoxDecoration(
                                color: Color.fromARGB(255, 232, 250, 237),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "+20%",
                                  style: TextStyle(
                                    fontFamily: 'Montserrat-Regular',
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Color.fromARGB(255, 0, 199, 82),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              child: SizedBox(
                width: 500,
                height: 0.9,
                child: Container(
                  color: Colors.grey.shade300,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        width: 100,
                        height: 200,
                        child: Text(
                          'SLA',
                          style: TextStyle(
                            fontFamily: 'Montserrat-Bold',
                            fontWeight: FontWeight.bold,
                            fontSize: 49,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 25),
                        width: 200,
                        height: 200,
                        child: new RadialChart(),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        width: 100,
                        height: 200,
                        child: Text(
                          this.widget.textrequest,
                          style: TextStyle(
                            fontFamily: 'Montserrat-Bold',
                            fontWeight: FontWeight.bold,
                            fontSize: 49,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
